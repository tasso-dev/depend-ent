import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.30"
    application
    id("com.github.jk1.dependency-license-report") version "1.17"
    id("org.jetbrains.dokka") version "1.5.30"
}

group = "dev.tasso"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<JavaCompile> {
    targetCompatibility = "1.8"
}

tasks.withType<Test> {
    useJUnitPlatform()
}

application {
    mainClass.set("MainKt")
}

tasks.withType<Wrapper> {
    gradleVersion = "7.2"
}

val thirdPartyLicenseFileName = "LICENSE-3RD-PARTY.txt"

licenseReport {
    //Include all dependencies, regardless of when/where it's used (runtime, test, etc.)
    configurations = com.github.jk1.license.LicenseReportExtension.ALL
    renderers = arrayOf<com.github.jk1.license.render.ReportRenderer>(
        com.github.jk1.license.render.TextReportRenderer(thirdPartyLicenseFileName)
    )
}

// This class is declared as a default Task and not a Copy Task, even though it is copying files. This is due to
// the unconventional behavior of the task "output" being a file generated outside of a build folder and in the project
// root. Since the target is the project root folder, a Copy task would always fail during up-to-date checks because
// it would attempt to acquire a lock on files within the the {project root}/build dir which are already locked.
//
tasks.register<Task>("generate3rdPartyLicenseDeclaration").configure {
    dependsOn("generateLicenseReport")

    doFirst {
        project.copy {

            // Copy only the license report from the output folder, ignoring embedded license files since the contents
            // are already included in the license file.
            from (files(tasks.named("generateLicenseReport"))) {
                include(thirdPartyLicenseFileName)
            }

            into(project.projectDir)

            // Replace the auto generated title with one stating it's the 3rd party license report
            filter{ line ->
                if (line.contains("Dependency License Report")) "Third Party Licenses for ${project.name}" else line
            }

            // We don't really care where we retrieve URLs from. Drop mention of POMs in the report.
            filter{ line ->
                line.replace("POM Project URL:", "Project URL:")
            }

            // We don't really care where we retrieve license info from. Drop mention of POMs in the report.
            filter{ line ->
                line.replace("POM License:", "License:")
            }

            // The license file should only change when 3rd party dependencies change.
            // Eliminate the line containing report generation time to prevent a unique report per build.
            filter{ line ->
                if (line.contains("This report was generated at")) "" else line
            }
        }
    }
}


